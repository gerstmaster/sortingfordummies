﻿using System;
using System.Collections.Generic;

using SortingForDummies.Core;
using SortingForDummies.Core.Sort;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SortingForDummies.Core.Test
{
	[TestClass]
	public class MergeSortTests
	{
		[TestMethod]
		public void Sort_1Item_Sorts()
		{
			var sorter = new MergeSort();

			var randomList = Lister.GenerateRandomList(1);
			var operationCount = 0L;

			var sortedList = sorter.Sort(randomList, ref operationCount);

			for (int index = 0; index < sortedList.Count - 1; index++)
			{
				Assert.IsTrue(sortedList[index] <= sortedList[index + 1]);
			}
		}
		[TestMethod]
		public void Sort_10Items_Sorts()
		{
			var sorter = new MergeSort();

			var randomList = Lister.GenerateRandomList(10);
			var operationCount = 0L;

			var sortedList = sorter.Sort(randomList, ref operationCount);

			for (int index = 0; index < sortedList.Count - 1; index++)
			{
				Assert.IsTrue(sortedList[index] <= sortedList[index + 1]);
			}
		}
		[TestMethod]
		public void Sort_100Items_Sorts()
		{
			var sorter = new MergeSort();

			var randomList = Lister.GenerateRandomList(100);
			var operationCount = 0L;

			var sortedList = sorter.Sort(randomList, ref operationCount);

			for (int index = 0; index < sortedList.Count - 1; index++)
			{
				Assert.IsTrue(sortedList[index] <= sortedList[index + 1]);
			}
		}
		[TestMethod]
		public void Sort_1000Items_Sorts()
		{
			var sorter = new MergeSort();

			var randomList = Lister.GenerateRandomList(1000);
			var operationCount = 0L;

			var sortedList = sorter.Sort(randomList, ref operationCount);

			for (int index = 0; index < sortedList.Count - 1; index++)
			{
				Assert.IsTrue(sortedList[index] <= sortedList[index + 1]);
			}
		}
		[TestMethod]
		public void Sort_10000Items_Sorts()
		{
			var sorter = new MergeSort();

			var randomList = Lister.GenerateRandomList(10000);
			var operationCount = 0L;

			var sortedList = sorter.Sort(randomList, ref operationCount);

			for (int index = 0; index < sortedList.Count - 1; index++)
			{
				Assert.IsTrue(sortedList[index] <= sortedList[index + 1]);
			}
		}
		[TestMethod]
		public void Sort_100000Items_Sorts()
		{
			var sorter = new MergeSort();

			var randomList = Lister.GenerateRandomList(100000);
			var operationCount = 0L;

			var sortedList = sorter.Sort(randomList, ref operationCount);

			for (int index = 0; index < sortedList.Count - 1; index++)
			{
				Assert.IsTrue(sortedList[index] <= sortedList[index + 1]);
			}
		}
		[TestMethod]
		public void Sort_1000000Items_Sorts()
		{
			var sorter = new MergeSort();

			var randomList = Lister.GenerateRandomList(1000000);
			var operationCount = 0L;

			var sortedList = sorter.Sort(randomList, ref operationCount);

			for (int index = 0; index < sortedList.Count - 1; index++)
			{
				Assert.IsTrue(sortedList[index] <= sortedList[index + 1]);
			}
		}
		[TestMethod]
		public void Sort_10000000Items_Sorts()
		{
			var sorter = new MergeSort();

			var randomList = Lister.GenerateRandomList(10000000);
			var operationCount = 0L;

			var sortedList = sorter.Sort(randomList, ref operationCount);

			for (int index = 0; index < sortedList.Count - 1; index++)
			{
				Assert.IsTrue(sortedList[index] <= sortedList[index + 1]);
			}
		}
	}
}
