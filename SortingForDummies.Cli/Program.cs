﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SortingForDummies.Core;
using SortingForDummies.Core.Sort;

namespace ConsoleApplication1
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("List Length: ");
			var input = Console.ReadLine();
			var listLength = Convert.ToInt32(input);

			var randomList = Lister.GenerateRandomList(listLength);

			Sort(new QuickSort(), randomList);

			Sort(new MergeSort(), randomList);

			Sort(new UnbalancedBinarySort(), randomList);

			Sort(new InsertionSort(), randomList);

			Sort(new BubbleSort(), randomList);

			Console.ReadLine();
		}

		static void Sort<T>(ISort sorter, IList<T> randomList) where T : IComparable<T>
		{
			try
			{
				var sortOperationCount = 0L;
				var sortStopWatch = new Stopwatch();
				sortStopWatch.Start();
				var sortedList = sorter.Sort(randomList, ref sortOperationCount);
				sortStopWatch.Stop();

				PrintSortStats(sorter.Type, sortStopWatch, sortOperationCount, sortedList);
			}
			catch(Exception ex)
			{
				PrintSortError(sorter.Type, ex);
			}
		}

		static void PrintSortStats<T>(string sortType, Stopwatch stopWatch, long operationCount, IList<T> sortedList) where T : IComparable<T>
		{
			Console.WriteLine("{0} Sort", sortType);

			Console.WriteLine("Sort is {0}", ValidateSort(sortedList) ? "Valid" : "Invalid");

			Console.WriteLine("Elapsed Time: {0} ms", stopWatch.ElapsedMilliseconds);

			Console.WriteLine("Operations: {0}", operationCount);

			Console.WriteLine();
		}

		static void PrintSortError(string sortType, Exception e)
		{
			Console.WriteLine("{0} Sort", sortType);

			Console.WriteLine("Errors: {0}", e.Message);

			Console.WriteLine("Stack Trace: {0}", e.StackTrace);

			Console.WriteLine();
		}

		static bool ValidateSort<T>(IList<T> sortedList) where T : IComparable<T>
		{
			var valid = true;

			for (int validationCounter = 0; validationCounter < sortedList.Count - 1 && valid; validationCounter++)
			{
				var thisOne = sortedList[validationCounter];
				var nextOne = sortedList[validationCounter + 1];

				if(thisOne.CompareTo(nextOne) > 0)
				{
					valid = false;
				}
			}

			return valid;
		}
	}
}
