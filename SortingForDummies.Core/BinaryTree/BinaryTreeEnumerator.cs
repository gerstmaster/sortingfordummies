﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingForDummies.Core.BinaryTree
{
	class BinaryTreeEnumerator<T> : IEnumerator<T>
		where T : IComparable<T>
	{
		private Stack<BinaryTreeNode<T>> stack;
		public T Current {get; private set; }
	
		public BinaryTreeEnumerator(BinaryTreeNode<T> root)
		{
			stack = new Stack<BinaryTreeNode<T>>();

			if(root != null)
			{
				PushLeftBranch(root);
			}
		}

		public void Dispose()
		{
			stack = null;
		}

		object System.Collections.IEnumerator.Current
		{
			get { return Current; }
		}

		public bool MoveNext()
		{
			var result = true;
			if(stack.Count == 0)
			{
				result = false;
			}
			else
			{
				var node = stack.Pop();
				Current = node.Value;

				if(node.Right != null)
				{
					PushLeftBranch(node.Right);
				}
			}

			return result;
		}

		public void Reset()
		{
			stack.Clear();
		}

		private void PushLeftBranch(BinaryTreeNode<T> node)
		{
			while (node != null)
			{
				stack.Push(node);
				node = node.Left;
			}
		}
	}
}
