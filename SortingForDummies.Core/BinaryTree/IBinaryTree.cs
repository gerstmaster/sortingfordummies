﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingForDummies.Core.BinaryTree
{
	public interface IBinaryTree<T> : IEnumerable<T>
		where T : IComparable<T>
	{
		BinaryTreeNode<T> Root { get; }

		void Add(T value);
	}
}
