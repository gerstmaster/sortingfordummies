﻿using System;
using System.Collections.Generic;

namespace SortingForDummies.Core.BinaryTree
{
	public class RedBlackBinaryTree<T> : IBinaryTree<T>
		where T : IComparable<T>
	{

		public BinaryTreeNode<T> Root { get; private set; }

		public void Add(T value)
		{
			throw new NotImplementedException();
		}

		public IEnumerator<T> GetEnumerator()
		{
			return new BinaryTreeEnumerator<T>(Root);
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
