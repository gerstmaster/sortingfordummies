﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingForDummies.Core.BinaryTree
{
	public class BinaryTreeNode<T>
	{
		public BinaryTreeNode(T value)
		{
			Value = value;
		}

		public T Value { get; private set; }

		public BinaryTreeNode<T> Left { get; set; }

		public BinaryTreeNode<T> Right { get; set; }
	}
}
