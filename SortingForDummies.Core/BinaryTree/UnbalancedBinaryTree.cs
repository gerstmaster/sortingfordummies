﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SortingForDummies.Core.BinaryTree
{
	class UnbalancedBinaryTree<T> : IBinaryTree<T>
		where T : IComparable<T>
	{
		public BinaryTreeNode<T> Root { get; private set; }

		public void Add(T value)
		{
			var newNode = new BinaryTreeNode<T>(value);

			if(Root == null)
			{
				Root = newNode;
			}
			else
			{
				Add(Root, newNode);
			}
		}

		public IEnumerator<T> GetEnumerator()
		{
			return new BinaryTreeEnumerator<T>(Root);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private void Add(BinaryTreeNode<T> root, BinaryTreeNode<T> node)
		{
			if(root.Value.CompareTo(node.Value) > 0)
			{
				if(root.Left == null)
				{
					root.Left = node;
				}
				else
				{
					Add(root.Left, node);
				}
			}
			else
			{
				if(root.Right == null)
				{
					root.Right = node;
				}
				else
				{
					Add(root.Right, node);
				}
			}
		}
	}
}
