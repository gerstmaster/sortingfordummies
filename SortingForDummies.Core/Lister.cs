﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingForDummies.Core
{
	public static class Lister
	{
		public static IList<int> GenerateRandomList(int resultLength)
		{
			var randomizer = new Random();

			var randomList = new List<int>();

			while (randomList.Count < resultLength)
			{
				int maxValue;
				if(resultLength > Math.Sqrt(int.MaxValue))
				{
					maxValue = int.MaxValue;
				}
				else
				{
					maxValue = Convert.ToInt32(Math.Pow(resultLength, 2));
				}

				var next = randomizer.Next(maxValue);

				randomList.Add(next);
			}

			return randomList;
		}
	}
}
