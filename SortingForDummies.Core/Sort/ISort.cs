﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingForDummies.Core.Sort
{
	public interface ISort
	{
		IList<T> Sort<T>(IList<T> unsortedList, ref long operationCount) where T : IComparable<T>;

		string Type { get; }
	}
}
