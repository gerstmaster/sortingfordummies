﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingForDummies.Core.Sort
{
	public class MergeSort : ISort
	{
		public string Type
		{
			get
			{
				return "Merge";
			}
		}

		public IList<T> Sort<T>(IList<T> unsortedList, ref long operationCount) where T : IComparable<T>
		{
			var sortedList = new List<T>();

			if (unsortedList.Count > 1)
			{
				var leftList = new List<T>();
				var rightList = new List<T>();

				for (var splitCounter = 0; splitCounter < unsortedList.Count; splitCounter++)
				{
					if (splitCounter < (unsortedList.Count / 2))
					{
						leftList.Add(unsortedList[splitCounter]);
					}
					else
					{
						rightList.Add(unsortedList[splitCounter]);
					}
				}

				var sortedLeft = Sort(leftList, ref operationCount);
				var sortedRight = Sort(rightList, ref operationCount);

				sortedList.AddRange(Merge(sortedLeft, sortedRight, ref operationCount));
			}
			else
			{
				sortedList.AddRange(unsortedList);
			}

			return sortedList;
		}

		protected IList<T> Merge<T>(IList<T> leftList, IList<T> rightList, ref long operationCount) where T: IComparable<T>
		{
			var mergedList = new List<T>();

			var leftEnum = leftList.GetEnumerator();
			var rightEnum = rightList.GetEnumerator();

			var mergeComplete = false;

			if(leftEnum.MoveNext() == false)
			{
				while(rightEnum.MoveNext())
				{
					mergedList.Add(rightEnum.Current);
				}
				mergeComplete = true;
			}
				
			if(rightEnum.MoveNext() == false)
			{
				while(leftEnum.MoveNext())
				{
					mergedList.Add(leftEnum.Current);
				}
				mergeComplete = true;
			}

			while (mergeComplete == false)
			{
				operationCount++;

				if(leftEnum.Current.CompareTo(rightEnum.Current) > 0)
				{
					mergedList.Add(rightEnum.Current);

					if(rightEnum.MoveNext() == false)
					{
						mergedList.Add(leftEnum.Current);
						while(leftEnum.MoveNext())
						{
							mergedList.Add(leftEnum.Current);
						}
						mergeComplete = true;
					}
				}
				else
				{
					mergedList.Add(leftEnum.Current);

					if(leftEnum.MoveNext() == false)
					{
						mergedList.Add(rightEnum.Current);
						while(rightEnum.MoveNext())
						{
							mergedList.Add(rightEnum.Current);
						}
						mergeComplete = true;
					}
				}
			}

			return mergedList;
		}
	}
}
