﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SortingForDummies.Core.BinaryTree;

namespace SortingForDummies.Core.Sort
{
	public class UnbalancedBinarySort : ISort
	{
		public IList<T> Sort<T>(IList<T> unsortedList, ref long operationCount) where T : IComparable<T>
		{
			var sortedList = new List<T>();

			var unbalancedBinaryTree = new UnbalancedBinaryTree<T>();

			foreach(T item in unsortedList)
			{
				operationCount++; //??
				unbalancedBinaryTree.Add(item);
			}

			foreach(T item in unbalancedBinaryTree)
			{
				sortedList.Add(item);
			}

			return sortedList;
		}

		public string Type
		{
			get { return "Unbalanced Binary"; }
		}
	}
}
