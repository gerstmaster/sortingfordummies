﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingForDummies.Core.Sort
{
	public class BubbleSort : ISort
	{
		public string Type
		{
			get
			{
				return "Bubble";
			}
		}

		public IList<T> Sort<T>(IList<T> unsortedList, ref long operationCount) where T : IComparable<T>
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			var sortedList = new List<T>(unsortedList);

			bool didSwap;

			do
			{
				didSwap = false;

				if (stopwatch.Elapsed.Minutes > 2)
				{
					//giveUp
					break;
				}
				else
				{
					for (int swapCounter = 0; swapCounter < sortedList.Count - 1; swapCounter++)
					{
						operationCount++;
						var thisItem = sortedList[swapCounter];
						var nextItem = sortedList[swapCounter + 1];

						if (thisItem.CompareTo(nextItem) > 0)
						{
							sortedList.Remove(thisItem);
							sortedList.Insert(swapCounter + 1, thisItem);
							didSwap = true;
						}
					}
				}
			}
			while (didSwap);

			return sortedList;
		}
	}
}
