﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingForDummies.Core.Sort
{
	public class QuickSort : ISort
	{
		public string Type
		{
			get
			{
				return "Quick";
			}
		}

		public IList<T> Sort<T>(IList<T> unsortedList, ref long operationCount) where T : IComparable<T>
		{
			var sortedList = new List<T>();

			if (unsortedList.Count > 1)
			{
				var randomizer = new Random();
				var pivotIndex = randomizer.Next(unsortedList.Count);

				var pivot = unsortedList[pivotIndex];

				var lessThan = new List<T>();
				var greaterThan = new List<T>();
				var equalTo = new List<T>();

				foreach (T item in unsortedList)
				{
					operationCount++;
					if (pivot.CompareTo(item) > 0)
					{
						lessThan.Add(item);
					}
					else if (pivot.CompareTo(item) < 0)
					{
						greaterThan.Add(item);
					}
					else
					{
						equalTo.Add(item);
					}
				}

				var sortedLessThan = Sort(lessThan, ref operationCount);
				var sortedGreaterThan = Sort(greaterThan, ref operationCount);

				sortedList.AddRange(sortedLessThan);
				sortedList.AddRange(equalTo);
				sortedList.AddRange(sortedGreaterThan);
			}
			else
			{
				sortedList.AddRange(unsortedList);
			}

			return sortedList;
		}
	}
}
