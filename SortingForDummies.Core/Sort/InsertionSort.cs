﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingForDummies.Core.Sort
{
	public class InsertionSort : ISort
	{
		public string Type
		{
			get
			{
				return "Insertion";
			}
		}

		public IList<T> Sort<T>(IList<T> unsortedList, ref long operationCount) where T : IComparable<T>
		{
			var stopWatch = new Stopwatch();
			stopWatch.Start();

			var sortedList = new List<T>();

			foreach(T item in unsortedList)
			{
				if(stopWatch.Elapsed.Minutes > 2)
				{
					//give up
					break;
				}

				if (sortedList.Count == 0)
				{
					sortedList.Add(item);
				}
				else
				{
					bool inserted = false;
					for (int insertionCounter = 0; (insertionCounter < sortedList.Count) && (inserted == false); insertionCounter++)
					{
						var sortedItem = sortedList[insertionCounter];

						operationCount++;
						if(item.CompareTo(sortedItem) < 0)
						{
							sortedList.Insert(insertionCounter, item);
							inserted = true;
						}
					}
				}
			}

			return sortedList;
		}
	}
}
